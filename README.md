# gmssl
> 结合 https://github.com/tfjoc/gmtls 和 https://github.com/piligo/gmssl 修改而来

## 调试
WireShark:
((ip.dst == 192.168.1.22 && ip.src == 192.168.1.20) || (ip.dst == 192.168.1.20 && ip.src == 192.168.1.22)) && tcp.port==44330 && tcp.len>0

TASSL v1.0.2o
tassl102 s_server -accept 44330 -CAfile sm2Certs/CA.pem -cert sm2Certs/SS.pem -enc_cert sm2Certs/SE.pem -msg -cntls
tassl102 s_client -connect 192.168.1.20:44330 -cntls -msg -cipher ECC-SM4-SM3


GMSSL v1.1.0d
gmssl s_server -accept 44330 -key sm2Certs/SS.key.pem -cert sm2Certs/SS.cert.pem -dkey sm2Certs/SE.key.pem -dcert sm2Certs/SE.cert.pem -CAfile sm2Certs/CA.cert.pem -msg -debug -state -gmtls -www

gmssl s_client -connect 192.168.1.20:44330 -key sm2Certs/CS.key.pem -cert sm2Certs/CS.cert.pem -CAfile sm2Certs/CA.cert.pem -msg -debug -gmtls -state
gmssl s_client -connect 192.168.1.22:44330 -gmtls -debug -state -msg


## 前言

https://gitee.com/gogm/gmssl

gmssl/gmtls  GM TLS/SSL Based on Golang (基于国密算法的TLS/SSL代码库)

## 说明

```
详细的使用参考 
1、[使用样例]https://github.com/piligo/gmssl-sample
2、[国密详细分析说明]https://github.com/piligo/gmssldoc
```

## 算法的详细交互过程

```
详细的交互文档见 项目 doc目录里面文档
doc/国密SSL交互的过程.Md
doc/TASSL环境准备.md
```





